use std::fs::File;
use std::error::Error;
use std::io::prelude::*;
use std::process;

pub struct Config {
    brt_val: i16,
    bmax_path: String,
    bnow_path: String,
}

impl Config {
    //TODO Implement set_val switch, and option
    pub fn new(mut args: std::env::Args) -> Result<Config, &'static str> {

        args.next();
        let brt_val = match args.next() {
                Some(arg) => arg,
                None      => {
                    eprintln!("Invalid Brightness value!");
                    print_usage();
                    process::exit(1);
                }
        };
        let brt_val: i16 = match brt_val.trim()
            .parse() {
            Ok(num) => num,
            Err(e)  => {
                eprintln!("Application Error : {}", e);
                process::exit(1);
            }
        };
        let (bmax_path, bnow_path) = match args.next() {
            Some(arg) => (arg.clone() + "/max_brightness", arg + "/brightness"),
            None      => {
                eprintln!("Device files not found in specified path!");
                process::exit(1);
            }
        };
        return Ok(Config{brt_val, bmax_path, bnow_path})
    }
}

pub fn run(config: Config) -> Result<(), Box<Error>> {

    /* Get max_brightness */
    let mut bmax = String::new();
    {
        let mut fbmax = File::open(&config.bmax_path)?;
        fbmax.read_to_string(&mut bmax)?;
    }
    let bmax: i16 = match bmax.trim()
        .parse::<i16>() {
            Ok(num) => num,
            Err(e)  => {
                eprintln!("Error Reading value from max_brightness!\
                           \nApplication Error : {}", e);
                process::exit(1);
            }
        };
    /* Get current brightness */
    let mut bnow = String::new();
    {
        let mut fbnow = File::open(&config.bnow_path)?;
        fbnow.read_to_string(&mut bnow)?;
    }
    let bnow: i16 = match bnow.trim()
        .parse::<i16>() {
            Ok(num) => num,
            Err(e)  => {
                eprintln!("Error Reading Current value of brightness!\
                          \nApplication Error : {}", e);
                process::exit(1);
            }
        };
    let mut final_brt: i16 = bnow + ((config.brt_val*bmax)/100);

    if final_brt > bmax {
        final_brt = bmax;
    } else if final_brt < 0 {
        final_brt = 0;
    }
    {
        let mut fbnow = File::create(&config.bnow_path)?;
        fbnow.write_all(final_brt.to_string().as_bytes())?;
    }
    return Ok(())
}

pub fn print_usage() {
    eprintln!("USAGE:\n\tsetbright [% Inc/Dec Brightness Value]\
               [Path To Backlight DIR]\n\nBrightness value : \
              \nPositive : Increment Brightness\
              \nNegative : Decrement_Brightness");
}
